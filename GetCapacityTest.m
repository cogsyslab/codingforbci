acc = .9:-.1:.6;
numSym = length(acc);
offDiag = (ones(numSym) - eye(numSym));
probYgivenX = diag(acc) + offDiag * diag((1-acc) / (numSym - 1));

[MI, probX] = GetCapacity(probYgivenX)