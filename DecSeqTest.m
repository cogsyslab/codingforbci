%% parameters
clearvars; clc;

minDecProb = .9;

numSourceMsg = 50;
numChannelMsg = 6;
confusionMatrixAcc = .9;
minMaxNumTrials = [10, 20];
discardThresh = .02;

% confidence of chanSymbol in Simulate (see DecSeq.Simulate)
conf = .9;

%'random', 'maxInfo', 'uniform', 'sequential', 'huffman'
codeType = 'maxInfo';

% 'random' or 'perfect'
%
% NOTE: do not use recursive tree style methods with trialType = 'random'
% as it will take them a long time to converge on a decision
trialType = 'perfect';

%% build confusion matrix
probYgivenX = ones(numChannelMsg)*(1-confusionMatrixAcc)/(numChannelMsg-1);
probYgivenX = eye(numChannelMsg)*confusionMatrixAcc + ...
    probYgivenX - diag(diag(probYgivenX));

%% build a priori distribution
randDistribution = @(size)(diff([0; sort(rand(size-1,1),'ascend'); 1]));

probM = randDistribution(numSourceMsg);

%% build code object
switch codeType
    case 'random'
        codeObj = RandomCode('probYgivenX',probYgivenX);
    case 'maxInfo'
        codeObj = MaxInfoCode('probYgivenX', probYgivenX, ...
            'mode', 'fast');
    case 'uniform'
        codeObj = UniformCode('probYgivenX',probYgivenX);
    case 'sequential'
        codeObj = SequentialCode('probYgivenX',probYgivenX);
    case 'huffman'
        codeObj = HuffmanCode('probYgivenX',probYgivenX);
end

%% build SequenceManagerObj
msgIdxTarget = randi(numSourceMsg);

decSeqObj = DecSeq(codeObj,...
    'minDecProb', minDecProb, ...
    'minMaxNumTrials', minMaxNumTrials, ...
    'discardThresh', discardThresh);

decSeqObjSim = DecSeq(codeObj,...
    'minDecProb', minDecProb);

%% test
decSeqObj.Init(probM, ...
    'msgIdxTarget', msgIdxTarget);

while ~decSeqObj.done
    % perform trial (stimulate / classify)
    switch trialType
        case 'perfect'
            probX = zeros(numChannelMsg,1);
            probX(decSeqObj.codeAll(decSeqObj.msgIdxTarget,end)) = 1;
        case 'random'
            probX = randDistribution(numChannelMsg);
    end
    
    % increment trial
    decSeqObj.Update(probX);
end

decSeqObjSim.Simulate(...
    'probM', probM, ...
    'msgIdxTarget', msgIdxTarget, ...
    'conf', conf);

%% display
decSeqObj %#ok<NOPTS>
decSeqObjSim %#ok<NOPTS>