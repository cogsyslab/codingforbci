classdef DecSeq < matlab.mixin.Copyable
    % this object manages the bayesian updating of a decision sequence of
    % BCI trials.  Specifically, it computes the a posteriori distribution
    % over the source messages (M) after obtaining brain symbols (X).  It
    % requires:
    %   P_M - aPriori distribution over M
    %   C(M) - a code which maps M -> X (see any DMC code)
    %   P_{\hat{X}|X} = confusionMatrix (encapsulated in DMC code)
    %
    % METHODS:
    %   Init     = initializes a decSeq
    %   Update   = given probY (from classifier) update probM
    %   Simulate = draw samples from expected probY (given that we know 
    %              ground truth M) and update until decision reached
    
    properties
        % 1x1 DMCcode (see DMCcode), maps a set of source messages (probM)
        % to a set of brainSignals X by building a code
        codeObj
        
        % 1x1 confidence threshold, make a decision when 
        % max(P_M) > minDecProb
        minDecProb
        
        % scalar, idx of srcMsg estimate (if a decision has been reached)
        msgIdxEstimate
        
        % scalar, idx of target
        msgIdxTarget
        
        % [numSrcMsg x numTrials (+1)] idx of brain signal associated with
        % each sourceMsg.  (+1) indicates that this matrix contains the
        % 'next' trial's code so long as a decision hasn't yet been made
        codeAll
        
        % [numBrainSymbols x numTrials] prob of brain symbol (estimates)
        probY
        
        % [numSrcMsg x numTrials + 1] prob of srcMsg before and after each
        % trial (fencepost counting between probY and probM)
        probM
        
        % [numSrcMsg x numTrials + 1] same as probM, but keeps discarded
        % probs
        probMall
        
        % [numSrc x numBrainSymbols x numTrials] conditional
        probMgivenY
        
        % [numTrials x 1] struct which contains data associated with each
        % trial (EEG or eyeTrack)
        dataStruct
        
        % discards a possibility if reduced below this threshold
        discardThresh
        
        % minimum number of trials to perform before discarding a src msg
        discardMinNumTrials
        
        % multiplicative growth rate of discard threshold
        discardThreshGrow
    end
    
    properties (Dependent)
        done         % whether or not sequence has made a decision
        
        correct      % whether or not decision is correct or not
        
        numTrials    % the idx of the active trial (or last trial if decSeq
        %              is done)
    end
    
    properties (Hidden = true)
        % logical, determines how a decision tree style code (sequential or
        % huffman) makes a decision.  if true, a decision is chosen
        % according as the decision tree's current leaf, otherwise chosen
        % as max probM given all evidence when any leaf is reached
        naiveFlag
        
        % [min, max] number of trials in a decision sequence, the first
        % element, set to 1 disables auto type.  the last element sets done
        % flag to 1 if numTrials exceeds max (and correct to nan)
        minMaxNumTrials
    end
    
    methods
        function self = DecSeq(codeObj,varargin)
            % constructor
            if ~nargin
                return % allows preallocation of SequenceManager arrays
            end
            
            validateConfThresh = @(x)(isscalar(x) && ...
                x > -1e-6 && x < 1 + 1e-6);
            validateCode = @(x)(isa(x,'DMCcode'));
            
            p = inputParser;
            p.addRequired('codeObj', validateCode);
            p.addParameter('minDecProb',.95,validateConfThresh);
            p.addParameter('naiveFlag', false, @islogical);
            p.addParameter('minMaxNumTrials', [0, nan]);
            p.addParameter('discardMinNumTrials', 2, @isscalar);
            p.addParameter('discardThresh', 0, @isscalar);
            p.addParameter('discardThreshGrow', 1, @isscalar);
            p.parse(codeObj,varargin{:});
            
            self.codeObj = p.Results.codeObj;
            self.minDecProb = p.Results.minDecProb;
            self.naiveFlag = p.Results.naiveFlag;
            self.minMaxNumTrials = p.Results.minMaxNumTrials;
            self.dataStruct = struct;
            
            self.discardThresh = p.Results.discardThresh;
            self.discardMinNumTrials = p.Results.discardMinNumTrials;   
            self.discardThreshGrow = p.Results.discardThreshGrow;
                        
            if self.minDecProb < .5
                warning('minDecProb < .5, unique estimate not certain');
            end
            
            isDtree = @(x)(ismember(class(x), ...
                {'SequentialCode', 'HuffmanCode'}));
            if self.naiveFlag && ~isDtree(self.codeObj)
                self.naiveFlag = false;
                warning('naive mode only valid in decision tree style codes, using full bayes');
            end
            
            if self.naiveFlag
                self.minDecProb = nan;
            end
            
            issquare = @(x)(ismatrix(x) && (size(x,1) == size(x,2)));
            
            if ~issquare(self.codeObj.probYgivenX)
                error('must have square confusion matrix');
            end
        end
        
        function Discard(self)
            % update probMall
            self.probMall(:, end + 1) = self.probM(:, end);
            
            % only discard if minNumTrial has been reached
            if self.numTrials < self.discardMinNumTrials
                return
            end
            
            % discard prob < thresh
            mask = self.probM(:, end) < self.discardThresh;
            self.probM(mask, end) = 0;
            
            % re normalize
            if sum(self.probM(:, end))
                self.probM(:, end) = self.probM(:, end) / ...
                    sum(self.probM(:, end));
            else
               % all msgs discarded, set to max from prev trial
               [~, idx] = max(self.probMall(:, end));
               self.probM(idx, end) = 1;
            end
                
            
            % discard thresh grows
            self.discardThresh = self.discardThresh * self.discardThreshGrow;
        end
        
        function [code, activeIconIdx] = getNextCode(self)
           % returns the next code (used for graphics).  if
           % codeObj.numSrcCombine is 0 (or does not exist) it is simply
           % the code vector. otherwise it hides all combined src icons
           % (code = 0) and shows the combined src icon (which is
           % associated with combinUserMsgIdx)
                      
           code = self.codeAll(:, end);
           activeIconIdx = true(length(code), 1);
           if ~isprop(self.codeObj, 'numSrcCombine') || ...
                   ~self.codeObj.numSrcCombine
               % src msgs aren't combined, return the naive code vector
               return
           end
           
           % get combinedSrcIdx, a vector of all srcIdx which are combined,
           % as well as combinUseMsgIdx, a scalar, which is the userMsg
           % they are all combined into
           [~, combinedSrcIdx] = sort(self.probM(:, end), 'ascend');
           combinedSrcIdx = combinedSrcIdx(1 : self.codeObj.numSrcCombine);
           combinUserMsgIdx = code(combinedSrcIdx(1));
           assert(all(code(combinedSrcIdx) == combinUserMsgIdx), ...
               'the lowest numSrcCombine msgs dont have same user symbol!');
           
           % build code which shows last icon as combined src icon in lieu
           % of combinedSrcIdx icons
           code(combinedSrcIdx) = 0;
           code(end + 1) = combinUserMsgIdx;
           activeIconIdx = find(code);
        end
        
        function val = get.done(self)
            val = ~isempty(self.msgIdxEstimate) || ...
                self.numTrials > self.minMaxNumTrials(2);
        end
        
        function val = get.numTrials(self)
            val = size(self.codeAll, 2);
        end
        
        function val = get.correct(self)
            if ~isempty(self.msgIdxTarget)
                val = self.msgIdxEstimate == self.msgIdxTarget;
            else
                val = [];
            end
        end
        
        function Init(self, probM, varargin)
            % takes an a priori distribution over src msg (probM) and
            % builds a code (mapping of srcMsg to brainSymbol)
            %
            % INPUT
            %   probM = [numSrcMsg x 1] a priori prob
            
            p = inputParser;
            p.addParameter('msgIdxTarget', []);
            p.parse(varargin{:});
            
            % determine if decision threshold has already been reached,
            % store as appropriate
            [maxProbM, maxProbMIdx] = max(probM);
            if maxProbM > self.minDecProb && ...
                    self.numTrials >= self.minMaxNumTrials(1)
                % decision threshold reached without trials...
                self.msgIdxEstimate = maxProbMIdx;
            else
                self.msgIdxEstimate = [];
            end
            
            self.probM = probM(:);
            self.msgIdxTarget = p.Results.msgIdxTarget;
            % reset probY (in case obj being re-used for new decSeq)
            self.probY = [];
            
            % build code
            [self.codeAll, self.probMgivenY] = self.codeObj.Build(probM);            
        end
        
        function Update(self, probY, varargin)
            % given probY update probM
            %
            % INPUT
            %   probY = [numBrainSym x 1] distribution over brain symbols
            %           (given by classifier)
            %   varargin = {(numData x 2) x 1} cell which contains meta
            %            data and its labels.  
            %   ex: varargin = {'eegData', [1, 2, ...], 
            %   'eyeGazeStruct', eyeGazeStruct}
            
            emptyStruct = struct;
            p = inputParser;
            p.addParameter('trialDataStruct', emptyStruct, @isstruct);
            p.parse(varargin{:});
            
            % store all fields from extra data
            dataFields = fields(p.Results.trialDataStruct);
            for fieldIdx = 1 : length(dataFields);
                field = dataFields{fieldIdx};
                self.dataStruct(self.numTrials).(field) = ...
                    p.Results.trialDataStruct.(field);
            end
            
            % store probBrainSymbol
            self.probY(:, end + 1) = probY;
            
            % compute latest probM given probY
            self.probM(:, end + 1) = self.probMgivenY(:, :, end) * probY;
            self.Discard;
                        
            % determine if decision has been reached
            [maxProbSource, maxProbSourceIdx] = max(self.probM(:,end));            
            switch class(self.codeObj)
                case {'SequentialCode','HuffmanCode'}
                    % decision tree method decisions are signaled by the
                    % codeObj which sends an all zero code with a nan in
                    % place of the naive decision
                    decisionLogical = false;
                otherwise
                    decisionLogical = maxProbSource > self.minDecProb && ...
                        self.numTrials >= self.minMaxNumTrials(1);
            end
            
            % if decision reached, store it.  if not, build new code
            if decisionLogical
                self.msgIdxEstimate = maxProbSourceIdx;
            else
                % build code
                % NOTE: probX is only used in sequential and
                % huffman codes, otherwise it is ignored
                [self.codeAll(:,end+1), ...
                    self.probMgivenY(:, :, end+1)] = self.codeObj.Build(...
                    self.probM(:,end), ...
                    'probX', probY(:,end));
                
                % determine if decision tree style codes made a decision
                % (not too graceful...)
                if any(isnan(self.codeAll(:,end)))
                    % decision tree code has made a decision
                    isDtree = @(x)(ismember(class(x), ...
                        {'SequentialCode', 'HuffmanCode'}));
                    assert(isDtree(self.codeObj));
                    if self.naiveFlag
                        % choose naively (whatever leaf we're at)
                        [~,self.msgIdxEstimate] = ...
                            max(isnan(self.codeAll(:,end)));
                    else
                        % choose max a posteriori
                        self.msgIdxEstimate = maxProbSourceIdx;
                    end
                    
                    % rm last "trial"
                    self.codeAll = self.codeAll(: , 1 : end - 1);
                    self.probMgivenY = self.probMgivenY(:, :, 1 : end - 1);
                end
            end
        end
                
        function Simulate(self, varargin)
            % simulates the selection of a srcMsg by sampling from
            % appropriate column of codeObj.probYgivenX (confusion matrix).
            % assumes that classifier makes estimate with confidence conf,
            % other prob mass spread uniformly.  process repeated until
            % decision made
            %
            % INPUT:
            %    probM        = [numSrcMsg x 1] initial distribution of 
            %                   srcMsg
            %    msgIdxTarget = scalar, idx of target in probM
            %    conf         = scalar in [0-1], confidence 'classifier' 
            %                   assigns to sampled user symbol
            
            p = inputParser;
            p.addParameter('probM', nan);
            p.addParameter('msgIdxTarget', nan);
            p.addParameter('conf', .9, @isscalar);
            p.parse(varargin{:});
            
            argPassed = @(x)(~ismember(x, p.UsingDefaults));
            
            if argPassed('probM') && argPassed('msgIdxTarget')
                self.Init(p.Results.probM, 'msgIdxTarget', p.Results.msgIdxTarget);
            elseif (size(self.probM, 2) ~= 1) || isempty(self.msgIdxTarget)
                error('call init or pass probM and msgIdxTarget before simulate');
            end
            
            assert(~self.done, 'decision already done');
            
            while ~self.done
                self.Update(pseudoClassify);
            end
            
            function probY = pseudoClassify
                % find dist of output brain symbol X
                xIdx = self.codeAll(self.msgIdxTarget, end);
                if ~xIdx 
                    % for seq and huffman, xIdx can = 0.  in this case we
                    % assume user signals first x
                    xIdx = 1;
                end
                probClassY = self.codeObj.probYgivenX(:, xIdx);
                numY = length(probClassY);
                
                % draw from distribution of output chan symbol
                drawSample = @(p)(randsample(1:length(p), 1, true, p));
                yIdx = drawSample(probClassY);
                
                % build distribution over output chan symbol
                probY = ones(length(probClassY), 1) * ...
                    (1 - p.Results.conf) / (numY - 1);
                probY(yIdx) = p.Results.conf;
            end
        end
        
    end
    
end

