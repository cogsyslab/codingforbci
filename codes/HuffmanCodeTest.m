%% HuffmanCodeTest.m
% This script builds a huffman tree plots the codeword length against the
% probM.  Note that higher probability messages are assigned to shorter
% codewords.

%% parameters
clearvars; clc;
numSrcMsg = 10;   
numX = 2;

%% build arbitrary input distributions - non square confusion matrix
randDist = @(L)(diff([0; sort(rand(L - 1, 1), 'ascend'); 1]));

for xIdx = numX : -1 : 1
    probYgivenX(:, xIdx) = randDist(numX);
end

probM = randDist(numSrcMsg);

%% test
% build random code object vector, initialize 1-9 as empty
huffmanCodeObj = HuffmanCode('probYgivenX',probYgivenX);

% build huffman code vector
[code, probMgivenY, MI] = huffmanCodeObj.Build(probM);

%% test - build one at a time (traverse tree)
targetMsgIdx = randi(numSrcMsg);
allCodes = code;
while any(code)
    % build probX as certain ground truth
    xTarget = code(targetMsgIdx);
    probX = zeros(numX,1);
    probX(xTarget) = 1;
    
    code = huffmanCodeObj.Build(probM,... 
        'probX', probX);
    
    if any(code)
        allCodes = [allCodes, code]; %#ok<AGROW>
    end
end

%% show graphs
codeLengths = sum(huffmanCodeObj.codebook > 0,2);

subplot(2,1,1);
bar(codeLengths);
xlabel('source message idx');
ylabel('codeword length');
subplot(2,1,2);
bar(probM);
xlabel('source message idx');
ylabel('probability');