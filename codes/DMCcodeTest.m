%% DMCcodeTest.m
% this script tests the basic functionality of the DMCcode superclass.
% specificially, it computes the mutual information between a perfect
% channel and a uniform distribution of source messages as well as
% "BuildAll" which counts through all the possible codes.

%% parameters
clearvars; clc;
numSrcMsg = 8;
numX = 4;       % necessarily = numY

%% build "dependant" parameters
probYgivenX = eye(numX);  % perfect channel

probM = ones(numSrcMsg,1) / numSrcMsg;  % uniform

code = repmat((1 : numX)',ceil(numSrcMsg / numX),1);
code = code(1 : numSrcMsg);

%% test
DMCcodeObj = DMCcode('probYgivenX', probYgivenX);

% constructor runs without input args
a(10) = DMCcode;

% should be 2 for default test (numSourceMsg = 8 and numChannelMsg = 4)
mutualInfo = DMCcodeObj.ComputeCodeStats([code,code],probM);
assert(isequal(mutualInfo, [2; 2]), 'invalid mutual info');

% count all codes, display counting image to verify
allCodes = DMCcodeObj.BuildAll(numSrcMsg);
imagesc(allCodes);