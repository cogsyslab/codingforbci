classdef MaxInfoCode < DMCcode
    % maximizes the mutual information between source message M and output
    % channel message Y.  Equivilantly, maximizes the expected decrease in
    % entropy from prior, P_M, to posterior, P_{M|Y}, distribution of
    % srcMsgs
    %
    % METHODS:
    %   Build = builds a code which maps each M to a single X (via one of 3
    %           modes, see below)
    %
    % NOTE:
    % exhaustive search isn't tractable for problems of scale and hillClimb
    % offers no garauntee of global optima ... still needs some work
    
    properties
        %'exhaustive', 'hillClimb' or 'fast', see Build method below
        mode
        
        % scalar, number of starting codes to init in hillClimb
        numHCstarts
        
        % scalar, number of src symbols which are constrained to have the
        % same user symbol (chosen as the numSrcCombine src symbols
        % with min prob)
        numSrcCombine
        
        crowdPenalty
    end
    
    properties (Access=protected)
        % [numSrc x 1] original prob distribution
        probMorig
    end
    
    methods
        function self = MaxInfoCode(varargin)
            self = self@DMCcode(varargin{:});
            
            % load and validates inputs or defaults
            p = inputParser;
            p.KeepUnmatched = true;
            p.addParameter('mode', 'hillClimb', ...
                @(x)(any(strcmp(x,{'hillClimb', 'exhaustive', 'fast'}))));
            p.addParameter('numHCstarts', 10, @isscalar);
            p.addParameter('numSrcCombine', 0, @isscalar);
            p.addParameter('crowdPenalty', 1e-10);
            p.parse(varargin{:});
            
            self.mode = p.Results.mode;
            self.numHCstarts = p.Results.numHCstarts;
            self.numSrcCombine = p.Results.numSrcCombine;
            self.crowdPenalty = p.Results.crowdPenalty;
            
            if self.numSrcCombine == 1
                % doesn't make sense to combine last 1 src symbol, we set
                % to 0 (allows us to use this value as a flag)
                self.numSrcCombine = 0;
            end
        end
        
        function [code, probMgivenY, MI, varargout] = Build(self, probM, varargin)
            % builds code
            % INPUT
            %  probM = [numSrcMsg x 1] "prior" distribution of srcMsgs
            % OUTPUT
            %  code  = [numSrcMsg x 1] idx of brainSymbol associated with
            %          each srcMsg
            
            isDist =  @(x)(abs(sum(abs(x)) - 1) < 1e-6);
            p = inputParser;
            p.KeepUnmatched = true;
            p.addRequired('probM', isDist);
            p.parse(probM, varargin{:});
            
            probM = self.combineSrc(probM);
            
            numSrcMsg = length(probM);
            
            switch self.mode
                case 'fast'
                    % finds the distribution which maximizes the capacity
                    % of the channel.  greedily attempts to replicate this
                    % distribution.
                    [~, probXopt] = GetCapacity(self.probYgivenX);
                    
                    [~, ordIdx] = sort(probM, 'descend');
                    codeAll = nan(numSrcMsg, 1);
                    for srcMsgIdx = 1 : numSrcMsg
                        
                        % assign the srcMsg to the VALID probXopt idx which
                        % 'needs' the most prob
                        [~, nextXIdx] = sort(probXopt, 'descend');
                        count = 0;
                        while true
                            count = count + 1;
                            codeAll(ordIdx(srcMsgIdx)) = nextXIdx(count);
                            if self.validateCodeVector(codeAll)
                                break
                            elseif count == length(nextXIdx)
                                error('invalid code computed via fast MMI code, try hillClimb');
                            end
                        end
                        probXopt(nextXIdx(count)) = ...
                            probXopt(nextXIdx(count)) - probM(ordIdx(srcMsgIdx));
                    end
                    
                    [MIreduced, ~] = self.ComputeCodeStats(codeAll, probM);
                    
                case 'exhaustive'
                    % choose optimal code by evaluating MI(M,Y) @ all
                    % codes possible
                    codeAll = self.BuildAll(numSrcMsg);
                    
                    % rm invalid codes
                    numCodes = size(codeAll, 2);
                    for codeIdx = numCodes : -1 : 1
                        isValid(codeIdx) = ...
                            self.validateCodeVector(codeAll(:, codeIdx));
                    end
                    codeAll = codeAll(:, isValid);
                    
                    [MIreduced, ~] = ...
                        self.ComputeCodeStats(codeAll,probM);
                case 'hillClimb'
                    % seel hillClimb
                    [codeAll, ~, MIreduced] = self.HillClimb(probM);
                otherwise
                    error('mode not recognized');
            end
            
            [codeAll, probM] = self.combineSrcUndo(codeAll, ...
                'clearFlag', true);
            
            [MI, probMgivenY] = self.ComputeCodeStats(codeAll, probM);
            
            equalWithinTol = @(a, b)(all(ismembertol(a(:), b(:))));
            assert(equalWithinTol(MI, MIreduced), 'MI computation error');
            
            [~, maxIdx] = max(MI);
            code = codeAll(:, maxIdx);
            probMgivenY = probMgivenY(:, :, maxIdx);
            varargout{1} = codeAll;
        end
        
    end
    
    methods (Hidden = true)
        
        function probM = combineSrc(self, probM)
            % constrain the search such that the last
            % self.numSrcCombine src symbols are given same user
            % symbol, returns sorted probM
            
            assert(isempty(self.probMorig), ...
                'call combineSrcUndo with clearFlag first');
            
            probM = probM(:);
            self.probMorig = probM;
            
            if ~self.numSrcCombine
                % no srcs combined, dont sort
                return
            end
            
            probM = sort(self.probMorig, 'descend');
            
            % probM is sorted in shorted to descending order except last
            % element, which is probSrcCombine
            numSrc = length(self.probMorig);
            numSrcReduced = numSrc - self.numSrcCombine;
            probM = [probM(1 : numSrcReduced); ...
                sum(probM(numSrcReduced + 1 : end))];
        end
        
        function [codeAllOut, probM] = combineSrcUndo(self, codeAll, varargin)
            % INPUT
            %   codeAll = [numSrcReduced x numCodes]
            % OUTPUT
            %   codeAll = [numSrc x numCodes]
            %   probM   = original probM
            
            assert(~isempty(self.probMorig), 'call combineSrc first');
            
            p = inputParser;
            p.addParameter('clearFlag', false, @islogical);
            p.addParameter('combineIconFlag', false, @islogical);
            p.parse(varargin{:});
            
            probM = self.probMorig;
            if p.Results.clearFlag
                self.probMorig = [];
            end
            
            if ~self.numSrcCombine
                % no srcs combined, dont
                codeAllOut = codeAll;
                return
            end
            
            [~, sortIdx] = sort(probM, 'descend');
            numSrc = length(self.probMorig);
            numSrcReduced = size(codeAll, 1);
            numCodes = size(codeAll, 2);
            
            % build codeAllOut, which has original idx order of build
            codeAllOut = nan(numSrc, numCodes);
            idxCombine = sortIdx(end - self.numSrcCombine + 1 : end);
            codeAllOut(sortIdx(1 : numSrcReduced - 1), :) = ...
                codeAll(1 : end - 1, :);
            
            if ~p.Results.combineIconFlag
                % code of length srcMsg, each srcMsg is encoded as its own
                % user msg
                for codeIdx = 1 : numCodes
                    codeAllOut(idxCombine, codeIdx) = codeAll(end, codeIdx);
                end
            else
                % code of length srcMSg + 1, combined srcMsg are nan, last
                % row (the combined srcMsg) contains user msg
                codeAllOut(end + 1, :) = codeAll(end, :);
            end
        end
        
        function [codeAll, probMgivenY, MI] = HillClimb(self, probM)
            % choose numHCstarts starting codes (randomly)
            % for each code update each m_i to maximize MI(M, Y),
            % continue updating one srcMsg at a time until a local
            % max is reached
            
            numSrcMsg = length(probM);
            numX = size(self.probYgivenX, 1);
            
            % in addition to the random initializations for hill
            % climb, we start with a uniformCode initialization as
            % well
            uniformCode = UniformCode.BuildStatic(probM, self.probYgivenX);
            if self.validateCodeVector(uniformCode)
                codeAll = uniformCode;
            else
                codeAll = [];
            end
            
            % find random code starting points (which are valid)
            while size(codeAll, 2) < self.numHCstarts
                codeVector = randi(numX, [numSrcMsg, 1]);
                if self.validateCodeVector(codeVector)
                    codeAll = [codeAll, codeVector]; %#ok<AGROW>
                end
            end
            
            % pre-allocate
            MI = NaN(self.numHCstarts, 1);
            probMgivenY = NaN(numSrcMsg, numX, self.numHCstarts);
            
            for codeIdx = self.numHCstarts : -1 : 1
                [codeAll(:, codeIdx), ...
                    MI(codeIdx), ...
                    probMgivenY(:, :, codeIdx)] = HillClimb(codeAll(:,codeIdx));
            end
            
            function [codeLocalMMI, MI, probMgivenY] = HillClimb(code)
                % performs hill climbing on a single code vector until a
                % local maximum is achieved.
                
                numX = size(self.probYgivenX, 1);
                
                % random srcMsgIdx at which we start optimizing code
                srcMsgIdx = randi(numSrcMsg);
                
                % to ensure local max, we optimize until we've gone through
                % numSourceMsg idxs without changing (then we're repeating
                % ourselves ...)
                loopSinceChange = 0;
                invalidCount = 0;
                while loopSinceChange < numSrcMsg
                    % build all possible steps from code
                    nextCodes = repmat(code, 1, numX);
                    nextCodes(srcMsgIdx, :) = 1 : numX;
                    for idx = numX : -1 : 1
                        if ~self.validateCodeVector(nextCodes(:, idx))
                            nextCodes(:, idx) = [];
                        end
                    end
                    
                    if isempty(nextCodes)
                        % increment active index (rolls over to 1)
                        srcMsgIdx = mod(srcMsgIdx, numSrcMsg)+1;
                        invalidCount = invalidCount + 1;
                        if invalidCount > numSrcMsg
                            break
                        end
                        continue
                    end
                    invalidCount = 0;
                    
                    % find mutual information of all possible next codes
                    nextMI = self.ComputeCodeStats(nextCodes, probM);
                    
                    [~, crowd] = mode(nextCodes); %#ok<CPROPLC>
                    crowdPenalty_ = crowd(:) * self.crowdPenalty;
                    
                    % find which channelInputMsgIdx maximizes
                    % currentMaxInfoCode for the activeSourceMsgIdx
                    [~, maxMIchannelInIdx] = max(nextMI - crowdPenalty_);
                    
                    if maxMIchannelInIdx == code(srcMsgIdx)
                        % currentMaxInfoCode is optimal in activeSourceMsgIdx
                        loopSinceChange = loopSinceChange + 1;
                    else
                        % currentMaxInfo is not optimal, change it in the
                        % direction which makes the biggest increase in
                        % MI(M,Y)
                        code(srcMsgIdx) = maxMIchannelInIdx;
                        loopSinceChange = 0;
                    end
                    
                    % increment active index (rolls over to 1)
                    srcMsgIdx = mod(srcMsgIdx, numSrcMsg)+1;
                end
                
                % prepare outputs
                [MI, probMgivenY] = self.ComputeCodeStats(code, probM);
                codeLocalMMI = code;
            end
        end
        
        function isValid = validateCodeVector(~, varargin)
            % other MMI subclasses have codeVector constraints, this method
            % is written to be overloaded to apply constraints in the hill
            % climbing search
            
            isValid = true;
        end
    end
    
end

