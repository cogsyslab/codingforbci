%% UniformCodeTest.m
% this script builds uniform codes, which attempt to find code vectors
% which distribution the source messages as uniformly as possible between
% channel input messages

%% parameters
clearvars; clc;
numSrcMsg = 10;
numX = 6;

%% build arbitrary input distributions - non square confusion matrix
randDistribution = @(size)(diff([0; sort(rand(size-1,1),'ascend'); 1]));

for channelMsgIdx = numX:-1:1
    probYgivenX(:,channelMsgIdx) = randDistribution(numX);
end

probM = randDistribution(numSrcMsg);

%% test
uniformCodeObj = UniformCode('probYgivenX',probYgivenX);

% build random code vector
[code, probMgivenY, MI, probX] = uniformCodeObj.Build(probM);


%% graph
% http://stackoverflow.com/questions/6406241/stacked-bar-graph-matlab
data = [code, probM];
barData = zeros(numX, numSrcMsg);
for srcIdx = 1 : numSrcMsg
    nextChannelIdx = code(srcIdx);
    nextIdx = sum(barData(nextChannelIdx, :) ~= 0) + 1;
    barData(nextChannelIdx, nextIdx) = probM(srcIdx);
end

hBar = bar((1 : numX), barData, 'stacked');
xlabel('channel input message idx');
ylabel('prob');
title('distribution of all source messages across input messages (code)');