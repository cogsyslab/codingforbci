%% RandomCodeTest.m
% this script builds random codes

%% parameters
clearvars; clc;
numSrcMsg = 10;
numX = 4;

%% build arbitrary input distributions - non square confusion matrix
randDistribution = @(size)(diff([0; sort(rand(size-1,1),'ascend'); 1]));

for channelMsgIdx = numX:-1:1
    probYgivenX(:,channelMsgIdx) = randDistribution(numX);
end

probM = randDistribution(numSrcMsg);

%% test
% build random code object vector, initialize 1-9 as empty
randomCodeObj = RandomCode('probYgivenX', probYgivenX);

% build random code vector
[code, probMgivenY, MI] = randomCodeObj.Build(probM);

