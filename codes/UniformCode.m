classdef UniformCode < DMCcode
    % splits the prob of srcMsgs as uniformly as possible across X without
    % regard to the confusion matrix
    %
    % METHODS:
    %   Build = given probM, assigns each M to an X
    
    methods
        function self = UniformCode(varargin)

            self = self@DMCcode(varargin{:});
        end
        
        function [code, probMgivenY, MI, varargout] = Build(self, probM, varargin)
            % builds code (wrapper for BuildStatic)
            % INPUT
            %  probM = [numSrcMsg x 1] "prior" distribution of srcMsgs
            % OUTPUT
            %  code  = [numSrcMsg x 1] idx of brainSymbol associated with
            %          each srcMsg
            
            [code, probX] = self.BuildStatic(probM, self.probYgivenX);
            
            % compute mutual info and probMgivenY
            [MI, probMgivenY] = self.ComputeCodeStats(code, probM);
            varargout{1} = probX;
        end
    end
    
    methods(Static)
        function [code, probX]  = BuildStatic(probM,probYgivenX)
            % build code. static method allows uniform's build to be called
            % without an instance, helpful for MaxInfoCode initialization
            % condition
            %
            % INPUT
            %  probM = [numSrcMsg x 1] "prior" distribution of srcMsgs
            % OUTPUT
            %  code  = [numSrcMsg x 1] idx of brainSymbol associated with
            %          each srcMsg
            
            isDist =  @(x)(abs(sum(abs(x))-1) < 1e-6);
            p = inputParser;
            p.KeepUnmatched = true;
            p.addRequired('probM', isDist);
            p.parse(probM);
            
            numSrcMsg = length(probM);
            numX = size(probYgivenX,2);
                       
            % from http://en.wikipedia.org/wiki/Partition_problem, see
            % greedy.  it isn't exact, but this is an NP hard problem.  we
            % assign highest prob M to X which currently has lowest prob
            % and repeat.
            code = NaN(numSrcMsg,1);
            probX = zeros(numX, 1);
            [~, srcMsgIdxDescend] = sort(probM, 'descend');
            for sourceMsgIdx = 1:numSrcMsg
                % find X with smallest assigned prob so far
                [~, leastProbXidx] = min(probX);
                
                % assign the smallest remaining unassigned source message
                % to smallestXIdx
                code(srcMsgIdxDescend(sourceMsgIdx)) = leastProbXidx;
                
                % track probX as we built it
                probX(leastProbXidx) = probX(leastProbXidx) + ...
                    probM(srcMsgIdxDescend(sourceMsgIdx));
            end
            
            % shuffle brain signal indices (avoids advantage/disadvantage
            % of strong/weak first brain signal and highest probability
            % source message)
            randMap = randperm(numX)';
            code = randMap(code);
        end
    end
end
