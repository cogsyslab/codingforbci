classdef RandomCode < DMCcode
    % assigns srcMsgs, M, to brain symbols, X, randomly
    %
    % METHODS:
    %   Build = builds a code which maps each M to a single X
    
    methods
        function self = RandomCode(varargin)
            self = self@DMCcode(varargin{:});
        end
        
        function [code, probMgivenY, MI] = Build(self, probM, varargin)
            % builds code
            % INPUT
            %  probM = [numSrcMsg x 1] "prior" distribution of srcMsgs
            % OUTPUT
            %  code  = [numSrcMsg x 1] idx of brainSymbol associated with
            %          each srcMsg
            
            isDist =  @(x)(abs(sum(abs(x)) - 1) < 1e-6);
            p = inputParser;
            p.KeepUnmatched = true;
            p.addRequired('probM', isDist);
            p.parse(probM, varargin{:});
            
            numSrcMsg = length(probM);
            numX = size(self.probYgivenX, 2);
            code = randi(numX, numSrcMsg, 1);
            
            % compute mutual info and probMgivenY
            [MI, probMgivenY] = self.ComputeCodeStats(code, probM);
        end
    end
    
    
end

